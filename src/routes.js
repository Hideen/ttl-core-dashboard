const routes = {
  '/': 'Home',
  '/dashboard': 'Dashboard',
  '/components': 'Components',
  '/components/request': 'Request',
  '/components/farmers': 'Farmers',
  '/components/fbos': 'FBOs',
  '/components/tractors': 'Tractors',
  '/components/maps': 'Maps',
  '/components/operators': 'Operators',
  '/components/tractorowners': 'Tractorowners',
  '/components/transactions': 'Transactions',
  '/components/maps': 'Maps',
};
export default routes;
