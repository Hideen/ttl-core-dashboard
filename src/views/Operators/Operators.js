import React, { Component } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import Tractors from "../Tractors/Tractors";
import JsonTable from 'react-json-table';

const items = [
    {  farmer: 'James Nana', location: 'Volo', organization: 'UMAR A. LATIF (KPANGMAGA FARMERS)' ,phone: '0266800577' ,acres:'20' },
    {  farmer: 'James Nana', location: 'Volo', organization: 'UMAR A. LATIF (KPANGMAGA FARMERS)' ,phone: '0266800577' ,acres:'20' },
    {  farmer: 'James Nana', location: 'Volo', organization: 'UMAR A. LATIF (KPANGMAGA FARMERS)' ,phone: '0266800577' ,acres:'20' },
    {  farmer: 'James Nana', location: 'Volo', organization: 'UMAR A. LATIF (KPANGMAGA FARMERS)' ,phone: '0266800577' ,acres:'20' },
    {  farmer: 'James Nana', location: 'Volo', organization: 'UMAR A. LATIF (KPANGMAGA FARMERS)' ,phone: '0266800577' ,acres:'20' },
    {  farmer: 'James Nana', location: 'Volo', organization: 'UMAR A. LATIF (KPANGMAGA FARMERS)' ,phone: '0266800577' ,acres:'20' },
    {  farmer: 'James Nana', location: 'Volo', organization: 'UMAR A. LATIF (KPANGMAGA FARMERS)' ,phone: '0266800577' ,acres:'20' },
    {  farmer: 'James Nana', location: 'Volo', organization: 'UMAR A. LATIF (KPANGMAGA FARMERS)' ,phone: '0266800577' ,acres:'20' },
];

const columns = [
    {key: 'farmer', label: 'Farmer Name'},
    {key: 'location', label: 'Location'},
    {key: 'organization', label: 'Organization'},
    {key:  'phone', label:'Phone'},
    {key:  'acres', label:'Acres'},
    ];

class Operators extends Component {
    constructor(props) {
        super(props);
        this.state = {
            large: false
        };

        this.toggleLarge= this.toggleLarge.bind(this);
    }

    toggleLarge() {
        this.setState({
            large: !this.state.large
        });
    }

    render() {
        return (
            <div className="animated fadeIn">
                <div className="row">
                    <div className="col-lg-12">
                        <div className="card">
                            <div className="card-header">
                                <i className="fa fa-align-justify"></i> Operators
                                <Button color="danger" className="table-action-btn" onClick={this.toggleLarge}>Add Operators</Button>
                            </div>

                            <div className="card-block">
                                <JsonTable rows={ items } columns={ columns } className={'table table-striped'} />
                                <ul className="pagination">
                                    <li className="page-item"><a className="page-link" href="#">Prev</a></li>
                                    <li className="page-item active">
                                        <a className="page-link" href="#">1</a>
                                    </li>
                                    <li className="page-item"><a className="page-link" href="#">2</a></li>
                                    <li className="page-item"><a className="page-link" href="#">3</a></li>
                                    <li className="page-item"><a className="page-link" href="#">4</a></li>
                                    <li className="page-item"><a className="page-link" href="#">Next</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <Modal isOpen={this.state.large} toggle={this.toggleLarge} className={'modal-lg ' + this.props.className}>
                    <ModalHeader toggle={this.toggleLarge}>Add Operator</ModalHeader>
                    <ModalBody>
                        <form action="" method="post">
                            <div className="form-group">
                                <div className="input-group">
                                    <span className="input-group-addon"><i className="fa fa-user"></i></span>
                                    <input type="text" id="username3" name="username3" placeholder="Operator Name" className="form-control"/>

                                </div>
                            </div>
                            <div className="form-group">
                                <div className="input-group">
                                    <span className="input-group-addon"><i className="fa fa-phone"></i></span>
                                    <input type="text" id="phonenumbe3" name="phonenumber3" placeholder="Phone Number" className="form-control"/>

                                </div>
                            </div>
                            <div className="form-group">
                                <div className="input-group">
                                    <span className="input-group-addon"><i className="fa fa-map-pin"></i></span>
                                    <input type="text" id="address3" name="address3" placeholder="House Address" className="form-control"/>

                                </div>
                            </div>
                            <div className="form-group">
                                <div className="input-group">
                                    <span className="input-group-addon"><i className="fa fa-legal"></i></span>
                                    <input type="text" id="license3" name="license3" placeholder="License" className="form-control"/>
                                </div>
                            </div>
                            <div className="from-group">
                                <div className="input-group">
                                    <span className="input-group-addon" ><i className="fa fa-truck" placeholder="Tractor"></i></span>
                                    <input type="hidden" id="Tractors"/>
                                    <select className="form-control "name="tractor" >
                                        <option disabled={Tractors}></option>
                                    </select>
                                </div>
                            </div>
                        </form>
                    </ModalBody>
                    <ModalFooter>
                        <Button color="primary" onClick={this.toggleLarge}>Save</Button>{' '}
                        <Button color="secondary" onClick={this.toggleLarge}>Cancel</Button>
                    </ModalFooter>
                </Modal>
            </div>

        )
    }
}

export default Operators;
