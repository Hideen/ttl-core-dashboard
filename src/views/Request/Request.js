import React, { Component } from 'react';
import JsonTable from 'react-json-table';

const items = [
  { id: '2312', farmer: 'James Nana', location: 'Volo', date: '2017-04-23 12:05PM' ,phone: '0266800577' },
  { id: '3424', farmer: 'James Nana', location: 'Volo', date: '2017-04-23 12:05PM' ,phone: '0266800577' },
  { id: '2712', farmer: 'James Nana', location: 'Volo', date: '2017-04-23 12:05PM' ,phone: '0266800577' },
  { id: '7757', farmer: 'James Nana', location: 'Volo', date: '2017-04-23 12:05PM' ,phone: '0266800577' },
  { id: '8866', farmer: 'James Nana', location: 'Volo', date: '2017-04-23 12:05PM' ,phone: '0266800577' },
  { id: '9778', farmer: 'James Nana', location: 'Volo', date: '2017-04-23 12:05PM' ,phone: '0266800577' },
  { id: '5677', farmer: 'James Nana', location: 'Volo', date: '2017-04-23 12:05PM' ,phone: '0266800577' },
  { id: '9008', farmer: 'James Nana', location: 'Volo', date: '2017-04-23 12:05PM' ,phone: '0266800577' },
];

const columns = [
    {key: 'id', label: 'Request ID'},
    {key: 'farmer', label: 'Farmer Name'},
    {key: 'location', label: 'Location'},
    {key: 'date', label: 'Date'},
    {key:  'phone', label:'Phone'},


];

class Tables extends Component {
  render() {
    return (
      <div className="animated fadeIn">
        <div className="row">
          <div className="col-lg-12">
            <div className="card">
              <div className="card-header">
                <i className="fa fa-align-justify"></i> Tractor Request
              </div>
              <div className="card-block">

                <JsonTable rows={ items } columns={ columns } className={'table table-striped'} />
                <ul className="pagination">
                  <li className="page-item"><a className="page-link" href="#">Prev</a></li>
                  <li className="page-item active">
                    <a className="page-link" href="#">1</a>
                  </li>
                  <li className="page-item"><a className="page-link" href="#">2</a></li>
                  <li className="page-item"><a className="page-link" href="#">3</a></li>
                  <li className="page-item"><a className="page-link" href="#">4</a></li>
                  <li className="page-item"><a className="page-link" href="#">Next</a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>

    )
  }
}

export default Tables;
