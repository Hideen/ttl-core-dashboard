import React, { Component } from 'react';
import {  Line } from 'react-chartjs-2';
import {  Progress } from 'reactstrap';

const brandPrimary =  '#20a8d8';
const brandSuccess =  '#4dbd74';
const brandInfo =     '#63c2de';
const brandDanger =   '#56f837';




// Main Chart

// convert Hex to RGBA
function convertHex(hex,opacity) {
  hex = hex.replace('#','');
  var r = parseInt(hex.substring(0,2), 16);
  var g = parseInt(hex.substring(2,4), 16);
  var b = parseInt(hex.substring(4,6), 16);

  var result = 'rgba('+r+','+g+','+b+','+opacity/100+')';
  return result;
}

//Random Numbers
function random(min,max) {
  return Math.floor(Math.random()*(max-min+1)+min);
}

var elements = 27;
var data1 = [];
var data2 = [];
var data3 = [];

for (var i = 0; i <= elements; i++) {
  data1.push(random(50,200));
  data2.push(random(80,100));
  data3.push(65);
}

const mainChart = {
  labels: ['M', 'T', 'W', 'T', 'F', 'S', 'S', 'M', 'T', 'W', 'T', 'F', 'S', 'S', 'M', 'T', 'W', 'T', 'F', 'S', 'S', 'M', 'T', 'W', 'T', 'F', 'S', 'S'],
  datasets: [
    {
      label: 'My First dataset',
      backgroundColor: convertHex(brandInfo,10),
      borderColor: brandInfo,
      pointHoverBackgroundColor: '#fff',
      borderWidth: 2,
      data: data1
    },
    {
      label: 'My Second dataset',
      backgroundColor: 'transparent',
      borderColor: brandSuccess,
      pointHoverBackgroundColor: '#fff',
      borderWidth: 2,
      data: data2
    },
    {
      label: 'My Third dataset',
      backgroundColor: 'transparent',
      borderColor: brandDanger,
      pointHoverBackgroundColor: '#fff',
      borderWidth: 1,
      borderDash: [8, 5],
      data: data3
    }
  ]
}

const mainChartOpts = {
  maintainAspectRatio: false,
  legend: {
    display: false
  },
  scales: {
    xAxes: [{
      gridLines: {
        drawOnChartArea: false,
      }
    }],
    yAxes: [{
      ticks: {
        beginAtZero: true,
        maxTicksLimit: 5,
        stepSize: Math.ceil(250 / 5),
        max: 250
      }
    }]
  },
  elements: {
    point: {
      radius: 0,
      hitRadius: 10,
      hoverRadius: 4,
      hoverBorderWidth: 3,
    }
  }
}

class Dashboard extends Component {

  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      dropdownOpen: false
    };
  }

  toggle() {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen
    });
  }

  render() {
    return (
        <div className="animated fadeIn">
          <div className="row">
            <div className="col-sm-8 col-md-3">
              <div className="card card-inverse card-info">
                <div className="card-block">
                  <div className="h1 text-muted text-right mb-2">
                    <i className="fa fa-tree"></i>
                  </div>
                  <div className="h4 mb-0">87500</div>
                  <small className="text-muted text-uppercase font-weight-bold">Acreage</small>
                  <Progress className="progress progress-white progress-xs mt-1" value="25" />
                </div>
              </div>
            </div>
            <div className="col-sm-8 col-md-3">
              <div className="card card-inverse card-success">
                <div className="card-block">
                  <div className="h1 text-muted text-right mb-2">
                    <i className="fa fa-users"></i>
                  </div>
                  <div className="h4 mb-0">28</div>
                  <small className="text-muted text-uppercase font-weight-bold">Operators</small>
                  <Progress className="progress progress-white progress-xs mt-1" value="25" />
                </div>
              </div>
            </div>
            <div className="col-sm-8 col-md-3">
              <div className="card card-inverse card-warning">
                <div className="card-block">
                  <div className="h1 text-muted text-right mb-2">
                    <i className="fa fa-money"></i>
                  </div>
                  <div className="h4 mb-0">GHS 60,570</div>
                  <small className="text-muted text-uppercase font-weight-bold">Total Income</small>
                  <Progress className="progress progress-white progress-xs mt-1" value="25" />
                </div>
              </div>
            </div>
            <div className="col-sm-8 col-md-3">
              <div className="card card-inverse card-primary">
                <div className="card-block">
                  <div className="h1 text-muted text-right mb-2">
                    <i className="fa fa-truck"></i>
                  </div>
                  <div className="h4 mb-0">28</div>
                  <small className="text-muted text-uppercase font-weight-bold">Registered Tractors</small>
                  <Progress className="progress progress-white progress-xs mt-1" value="25" />
                </div>
              </div>
            </div>
            <div className="col-sm-8 col-md-3">
              <div className="card card-inverse card-primary">
                <div className="card-block">
                  <div className="h1 text-muted text-right mb-2">
                    <i className="fa fa-truck"></i>
                  </div>
                  <div className="h4 mb-0">28</div>
                  <small className="text-muted text-uppercase font-weight-bold">Tractors Owners</small>
                  <Progress className="progress progress-white progress-xs mt-1" value="25" />
                </div>
              </div>
            </div>
            <div className="col-sm-8 col-md-3">
              <div className="card card-inverse card-secondary">
                <div className="card-block">
                  <div className="h1 text-muted text-right mb-2">
                    <i className="fa fa-user-secret"></i>
                  </div>
                  <div className="h4 mb-0">1238</div>
                  <small className="text-muted text-uppercase font-weight-bold">FBos</small>
                  <Progress className="progress progress-white progress-xs mt-1" value="25" />
                </div>
              </div>
            </div>

            <div className="col-sm-8 col-md-3">
              <div className="card card-inverse card-dropbox">
                <div className="card-block">
                  <div className="h1 text-muted text-right mb-2">
                    <i className="fa fa-users"></i>
                  </div>
                  <div className="h4 mb-0">1238</div>
                  <small className="text-muted text-uppercase font-weight-bold">Farmers</small>
                  <Progress className="progress progress-white progress-xs mt-1" value="25" />
                </div>
              </div>
            </div>

            <div className="col-sm-8 col-md-3">
              <div className="card card-inverse card-danger">
                <div className="card-block">
                  <div className="h1 text-muted text-right mb-2">
                    <i className="fa fa-pencil-square"></i>
                  </div>
                  <div className="h4 mb-0">1238</div>
                  <small className="text-muted text-uppercase font-weight-bold">Pending Request</small>
                  <Progress className="progress progress-white progress-xs mt-1" value="25" />
                </div>
              </div>
            </div>
            <div className="col-sm-8 col-md-3">
              <div className="card card-inverse card-success">
                <div className="card-block">
                  <div className="h1 text-muted text-right mb-2">
                    <i className="fa fa-toggle-on"></i>
                  </div>
                  <div className="h4 mb-0">1238</div>
                  <small className="text-muted text-uppercase font-weight-bold">Tractor Online</small>
                  <Progress className="progress progress-white progress-xs mt-1" value="25" />
                </div>
              </div>
            </div>
            <div className="col-sm-8 col-md-3">
              <div className="card card-inverse card-warning">
                <div className="card-block">
                  <div className="h1 text-muted text-right mb-2">
                    <i className="fa fa-money"></i>
                  </div>
                  <div className="h4 mb-0">GHS 60,570</div>
                  <small className="text-muted text-uppercase font-weight-bold">Cleared Amount</small>
                  <Progress className="progress progress-white progress-xs mt-1" value="25" />
                </div>
              </div>
            </div>
            <div className="col-sm-8 col-md-3">
              <div className="card card-inverse card-sportify">
                <div className="card-block">
                  <div className="h1 text-muted text-right mb-2">
                    <i className="fa fa-money"></i>
                  </div>
                  <div className="h4 mb-0">GHS 60,570</div>
                  <small className="text-muted text-uppercase font-weight-bold">Commesion</small>
                  <Progress className="progress progress-white progress-xs mt-1" value="25" />
                </div>
              </div>
            </div>

            <div className="col-sm-8 col-md-3">
              <div className="card card-inverse card-success">
                <div className="card-block">
                  <div className="h1 text-muted text-right mb-2">
                    <i className="fa fa-refresh"></i>
                  </div>
                  <div className="h4 mb-0">1238</div>
                  <small className="text-muted text-uppercase font-weight-bold">Number Request</small>
                  <Progress className="progress progress-white progress-xs mt-1" value="25" />
                </div>
              </div>
            </div>



            <div className="card">
            <div className="card-block">
              <div className="row">
                <div className="col-sm-5">
                  <h4 className="card-title mb-0">Tractor Request</h4>
                  <div className="small text-muted">November </div>
                </div>
                <div className="col-sm-7 hidden-sm-down">
                  <button type="button" className="btn btn-primary float-right"><i className="icon-cloud-download"></i></button>
                  <div className="btn-toolbar float-right" role="toolbar" aria-label="Toolbar with button groups">
                    <div className="btn-group mr-3" data-toggle="buttons" aria-label="First group">
                      <label className="btn btn-outline-secondary">
                        <input type="radio" name="options" id="option1"/> Day
                      </label>
                      <label className="btn btn-outline-secondary active">
                        <input type="radio" name="options" id="option2" defaultChecked/> Month
                      </label>
                      <label className="btn btn-outline-secondary">
                        <input type="radio" name="options" id="option3"/> Year
                      </label>
                    </div>
                  </div>
                </div>
              </div>
              <div className="chart-wrapper" style={{height: 300 + 'px', marginTop: 40 + 'px'}}>
                <Line data={mainChart} options={mainChartOpts} height={300}/>
              </div>
            </div>
            <div className="card-footer">
              <ul>
                <li>
                  <div className="text-muted">Tractors</div>
                  <strong>29703 </strong>
                  <Progress className="progress-xs mt-2" color="success" value="40" />
                </li>
                <li className="hidden-sm-down">
                  <div className="text-muted">FBOs</div>
                  <strong>24093 </strong>
                  <Progress className="progress-xs mt-2" color="info" value="20" />
                </li>
                <li>
                  <div className="text-muted">Operators</div>
                  <strong>78706</strong>
                  <Progress className="progress-xs mt-2" color="warning" value="60" />
                </li>
                <li className="hidden-sm-down">
                  <div className="text-muted">Tractor Owners</div>
                  <strong>22123</strong>
                  <Progress className="progress-xs mt-2" color="danger" value="80" />
                </li>
                <li className="hidden-sm-down">
                  <div className="text-muted">Pending Request</div>
                  <strong>4015</strong>
                  <Progress className="progress-xs mt-2" color="primary" value="40" />
                </li>
              </ul>
            </div>
          </div>

        </div>
      </div>



    )
  }
}

export default Dashboard;
