import React, { Component } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import Tractorowners from "../Tractorowners/Tractorowners";
import Avatar from 'react-avatar';

import JsonTable from 'react-json-table';

const items = [
    {  make: 'MF', owner: 'James Nana', chasis: 'HCCZ4030LGCG48310' ,engine: 'B117900' ,model:'TT4030',registeration:'VR 36-2017',year:'2017' },
    {  make: 'MF', owner: 'James Nana', chasis: 'HCCZ4030LGCG48310' ,engine: 'B117900' ,model:'TT4030',registeration:'VR 36-2017',year:'2017' },
    {  make: 'MF', owner: 'James Nana', chasis: 'HCCZ4030LGCG48310' ,engine: 'B117900' ,model:'TT4030',registeration:'VR 36-2017',year:'2017' },
    {  make: 'MF', owner: 'James Nana', chasis: 'HCCZ4030LGCG48310' ,engine: 'B117900' ,model:'TT4030',registeration:'VR 36-2017',year:'2017' },
    {  make: 'MF', owner: 'James Nana', chasis: 'HCCZ4030LGCG48310' ,engine: 'B117900' ,model:'TT4030',registeration:'VR 36-2017',year:'2017' },
    {  make: 'MF', owner: 'James Nana', chasis: 'HCCZ4030LGCG48310' ,engine: 'B117900' ,model:'TT4030',registeration:'VR 36-2017',year:'2017' },
    {  make: 'MF', owner: 'James Nana', chasis: 'HCCZ4030LGCG48310' ,engine: 'B117900' ,model:'TT4030',registeration:'VR 36-2017',year:'2017' },
    {  make: 'MF', owner: 'James Nana', chasis: 'HCCZ4030LGCG48310' ,engine: 'B117900' ,model:'TT4030',registeration:'VR 36-2017',year:'2017' },
    {  make: 'MF', owner: 'James Nana', chasis: 'HCCZ4030LGCG48310' ,engine: 'B117900' ,model:'TT4030',registeration:'VR 36-2017',year:'2017' },
];

const columns = [
    {key: 'make', label: 'Make'},
    {key: 'owner', label: 'Owner'},
    {key: 'chasis', label: 'Chasis Number'},
    {key:  'engine', label:'Engine Number'},
    {key:  'model', label:'Model'},
    {key:  'registeration', label:'Registeration'},
    {key:  'year', label:'Year'},
];

class Tractors extends Component {
    constructor(props) {
        super(props);
        this.state = {
            large: false
        };

        this.toggleLarge= this.toggleLarge.bind(this);
    }

    toggleLarge() {
        this.setState({
            large: !this.state.large
        });
    }

    render() {
        return (
            <div className="animated fadeIn">
                <div className="row">
                    <div className="col-lg-12">
                        <div className="card">
                            <div className="card-header">
                                <i className="fa fa-align-justify"></i>  Tractors
                                <Button color="danger" className="table-action-btn" onClick={this.toggleLarge}>Add Tractor</Button>
                            </div>

                            <div className="card-block">
                                <JsonTable rows={ items } columns={ columns } className={'table table-striped'} />
                                <ul className="pagination">
                                    <li className="page-item"><a className="page-link" href="#">Prev</a></li>
                                    <li className="page-item active">
                                        <a className="page-link" href="#">1</a>
                                    </li>
                                    <li className="page-item"><a className="page-link" href="#">2</a></li>
                                    <li className="page-item"><a className="page-link" href="#">3</a></li>
                                    <li className="page-item"><a className="page-link" href="#">4</a></li>
                                    <li className="page-item"><a className="page-link" href="#">Next</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <Modal isOpen={this.state.large} toggle={this.toggleLarge} className={'modal-lg ' + this.props.className}>
                    <ModalHeader toggle={this.toggleLarge}>Add Tractor</ModalHeader>
                    <ModalBody>
                        <form action="" method="post">
                            <div className="form-group">
                                <div className="input-group">
                                    <span className="input-group-addon"><i className="fa fa-truck"></i></span>
                                    <input type="text" id="username3" name="username3" placeholder="Tractor Make" className="form-control"/>

                                </div>
                            </div>
                            <div className="form-group">
                                <div className="input-group">
                                    <span className="input-group-addon"><i className="fa fa-registered"></i></span>
                                    <input type="text" id="phonenumbe3" name="phonenumber3" placeholder="Registration Number" className="form-control"/>
                                </div>
                            </div>
                            <div className="form-group">
                                <div className="input-group">
                                    <span className="input-group-addon"><i className="fa fa-braille"></i></span>
                                    <input type="number" id="model" name="model" placeholder="Model" className="form-control"/>
                                </div>
                            </div>
                            <div className="form-group">
                                <div className="input-group">
                                    <span className="input-group-addon"><i className="fa fa-chain"></i></span>
                                    <input type="number" id="model" name="model" placeholder="Chasis Number" className="form-control"/>
                                </div>
                            </div>
                            <div className="form-group">
                                <div className="input-group">
                                    <span className="input-group-addon"><i className="fa fa-calendar-minus-o"></i></span>
                                    <input type="text" id="Location3" name="location3" placeholder="Year" className="from-control"/>
                                </div>
                            </div>
                            <div className="from-group">
                                <div className="input-group">
                                    <span className="input-group-addon" ><i className="fa fa-user-secret" placeholder="Owner"></i></span>
                                    <input type="hidden" id="Tractorowner"/>
                                    <select className="form-control "name="tractor" >
                                        <option disabled={Tractorowners}></option>
                                    </select>
                                </div>
                            </div>
                        </form>

                    </ModalBody>
                    <ModalFooter>
                        <Button color="primary" onClick={this.toggleLarge}>Save</Button>{' '}
                        <Button color="secondary" onClick={this.toggleLarge}>Cancel</Button>
                    </ModalFooter>
                </Modal>
            </div>
        )
    }
}

export default Tractors;
