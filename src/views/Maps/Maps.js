import React, { Component } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import {Map, InfoWindow, Marker, GoogleApiWrapper} from 'google-maps-react';

const mapStyle = {
  width: '100%',
  height: '100%',
  position: 'inherit !important'
}

const mapContainerStyle = {
  height: '480px',
}

class Maps extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: false
    };

    this.toggle = this.toggle.bind(this);
  }

  toggle() {
    this.setState({
      modal: !this.state.modal
    });
  }

  render() {
      return (
        <div className="animated fadeIn">
          <div className="row">
              <div className="col-lg-12">
                  <div className="card">
                      <div className="card-header">
                          <i className="fa fa-align-justify"></i> TTL TRACTOR MAP
                      </div>

                      <div style={mapContainerStyle}>
                        <Map
                          google={this.props.google}
                          zoom={7}
                          style={mapStyle}
                          initialCenter={{
                            lat: 5.645407,
                            lng: -0.151562
                          }}
                          >
                          <Marker onClick={this.onMarkerClick} name={'Current location'} />
                          <Marker
                            title={'The marker`s title will appear as a tooltip.'}
                            name={'SOMA'}
                            position={{lat: 37.778519, lng: -122.405640}} />
                          <InfoWindow onClose={this.onInfoWindowClose}>
                              <div>
                                {/* <h1>{this.state.selectedPlace.name}</h1> */}
                              </div>
                          </InfoWindow>
                        </Map>
                      </div>
                  </div>
              </div>
          </div>

      </div>
    )
  }
}

// export default Maps;
export default GoogleApiWrapper({
  apiKey: ('AIzaSyDS8lyiECtcILX9jRJPnHfDOsZ68QXhGpE')
})(Maps)
