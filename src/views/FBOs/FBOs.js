import React, { Component } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import JsonTable from 'react-json-table';

const items = [
    {  farmer: 'James Nana', location: 'Volo', organization: 'UMAR A. LATIF (KPANGMAGA FARMERS)' ,phone: '0266800577' ,farmers:'20' },
    {  farmer: 'James Nana', location: 'Volo', organization: 'UMAR A. LATIF (KPANGMAGA FARMERS)' ,phone: '0266800577' ,farmers:'20' },
    {  farmer: 'James Nana', location: 'Volo', organization: 'UMAR A. LATIF (KPANGMAGA FARMERS)' ,phone: '0266800577' ,farmers:'20' },
    {  farmer: 'James Nana', location: 'Volo', organization: 'UMAR A. LATIF (KPANGMAGA FARMERS)' ,phone: '0266800577' ,farmers:'20' },
    {  farmer: 'James Nana', location: 'Volo', organization: 'UMAR A. LATIF (KPANGMAGA FARMERS)' ,phone: '0266800577' ,farmers:'20' },
    {  farmer: 'James Nana', location: 'Volo', organization: 'UMAR A. LATIF (KPANGMAGA FARMERS)' ,phone: '0266800577' ,farmers:'20' },
    {  farmer: 'James Nana', location: 'Volo', organization: 'UMAR A. LATIF (KPANGMAGA FARMERS)' ,phone: '0266800577' ,farmers:'20' },
    {  farmer: 'James Nana', location: 'Volo', organization: 'UMAR A. LATIF (KPANGMAGA FARMERS)' ,phone: '0266800577' ,farmers:'20' },
];

const columns = [
    {key: 'farmer', label: 'Farmer Name'},
    {key: 'location', label: 'Location'},
    {key:  'phone', label:'Phone'},
    {key:  'farmers', label:'No.Of Farmers'},


];
class FBOs extends Component {
    constructor(props) {
        super(props);
        this.state = {
            large: false
        };

        this.toggleLarge= this.toggleLarge.bind(this);
    }
    toggleLarge() {
        this.setState({
            large: !this.state.large
        });
    }
    render() {
        return (
            <div className="animated fadeIn">
                <div className="row">
                    <div className="col-lg-12">
                        <div className="card">
                            <div className="card-header">
                                <i className="fa fa-align-justify"></i> Combined All Table
                                <Button color="danger" className="table-action-btn" onClick={this.toggleLarge}>Add FBOs</Button>
                            </div>
                            <div className="card-block">
                                <div className="card-block">
                                    <JsonTable rows={ items } columns={ columns } className={'table table-striped'} />
                                    <ul className="pagination">
                                        <li className="page-item"><a className="page-link" href="#">Prev</a></li>
                                        <li className="page-item active">
                                            <a className="page-link" href="#">1</a>
                                        </li>
                                        <li className="page-item"><a className="page-link" href="#">2</a></li>
                                        <li className="page-item"><a className="page-link" href="#">3</a></li>
                                        <li className="page-item"><a className="page-link" href="#">4</a></li>
                                        <li className="page-item"><a className="page-link" href="#">Next</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                <Modal isOpen={this.state.large} toggle={this.toggleLarge} className={'modal-lg ' + this.props.className}>
                    <ModalHeader toggle={this.toggleLarge}>Add FBOs</ModalHeader>
                    <ModalBody>
                        <form action="" method="post">
                            <div className="form-group">
                                <div className="input-group">
                                    <span className="input-group-addon"><i className="fa fa-user"></i></span>
                                    <input type="text" id="username3" name="username3" placeholder="Leader Name" className="form-control"/>
                                </div>
                            </div>
                            <div className="form-group">
                                <div className="input-group">
                                    <span className="input-group-addon"><i className="fa fa-phone"></i></span>
                                    <input type="text" id="phonenumbe3" name="phonenumber3" placeholder="Phone Number" className="form-control"/>

                                </div>
                            </div>
                            <div className="form-group">
                                <div className="input-group">
                                    <span className="input-group-addon"><i className="fa fa-mars-double"></i></span>
                                    <input type="text" id="gender3" name="gender3" placeholder="Gender" className="form-control"/>

                                </div>
                            </div>
                            <div className="form-group">
                                <div className="input-group">
                                    <span className="input-group-addon"><i className="fa fa-tree"></i></span>
                                    <input type="number" id="farmers3" name="farmers3" placeholder="Number of farmers" className="form-control"/>

                                </div>
                            </div>
                            <div className="form-group">
                                <div className="input-group">
                                    <span className="input-group-addon"><i className="fa fa-map-marker"></i></span>
                                    <input type="text" id="Location3" name="location3" placeholder="Location" className="from-control"/>

                                </div>
                            </div>
                        </form>

                    </ModalBody>
                    <ModalFooter>
                        <Button color="primary" onClick={this.toggleLarge}>Save</Button>{' '}
                        <Button color="secondary" onClick={this.toggleLarge}>Cancel</Button>
                    </ModalFooter>
                </Modal>
            </div>
            </div>
        )
    }
}

export default FBOs;
