import React, { Component } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

import JsonTable from  'react-json-table';

const items =[
    {name:'Baba Amado',type: 'Company',phone: '0266800577', location:'Yendi',numberOftractors:'2'},
    {name:'Baba Amado',type: 'Company',phone: '0266800577', location:'Yendi',numberOftractors:'6'},
    {name:'Baba Amado',type: 'Company',phone: '0266800577', location:'Yendi',numberOftractors:'5'},
    {name:'Baba Amado',type: 'Company',phone: '0266800577', location:'Yendi',numberOftractors:'3'},
    {name:'Baba Amado',type: 'Company',phone: '0266800577', location:'Yendi',numberOftractors:'7'},
    {name:'Baba Amado',type: 'Company',phone: '0266800577', location:'Yendi',numberOftractors:'10'},


];
const columns =[
    {key:'name', label:'Owner Name'},
    {key:'type', label:'Type'},
    {key:'phone', label:'Phone Number'},
    {key:'location',label:'Location'},
    {key:'numberOftractors',label:'Number Of Tractors'},
];

class Tractorowners extends Component {
    constructor(props) {
        super(props);
        this.state = {
            large: false
        };

        this.toggleLarge= this.toggleLarge.bind(this);
    }

    toggleLarge() {
        this.setState({
            large: !this.state.large
        });
    }

    render() {
        return (
            <div className="animated fadeIn">
                <div className="row">
                    <div className="col-lg-12">
                        <div className="card">
                            <div className="card-header">
                                <i className="fa fa-align-justify"></i> Tractor Owners
                                <Button color="danger" className="table-action-btn" onClick={this.toggleLarge}>Owner</Button>
                            </div>

                            <div className="card-block">
                                <JsonTable rows={ items } columns={ columns } className={'table table-striped'} />
                                <ul className="pagination">
                                    <li className="page-item"><a className="page-link" href="#">Prev</a></li>
                                    <li className="page-item active">
                                        <a className="page-link" href="#">1</a>
                                    </li>
                                    <li className="page-item"><a className="page-link" href="#">2</a></li>
                                    <li className="page-item"><a className="page-link" href="#">3</a></li>
                                    <li className="page-item"><a className="page-link" href="#">4</a></li>
                                    <li className="page-item"><a className="page-link" href="#">Next</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <Modal isOpen={this.state.large} toggle={this.toggleLarge} className={'modal-lg ' + this.props.className}>
                    <ModalHeader toggle={this.toggleLarge}>Add Owner</ModalHeader>
                    <ModalBody>
                        <form action="" method="post">
                        <div className="form-group">
                            <div className="input-group">
                                <span className="input-group-addon"><i className="fa fa-user-secret"></i></span>
                                <input type="text" id="username3" name="username3" placeholder="Owner Name" className="form-control"/>

                            </div>
                        </div>
                        <div className="form-group">
                            <div className="input-group">
                                <span className="input-group-addon"><i className="fa fa-phone"></i></span>
                                <input type="text" id="phonenumbe3" name="phonenumber3" placeholder="Phone Numbers" className="form-control"/>

                            </div>
                        </div>
                        <div className="form-group">
                            <div className="input-group">
                                <span className="input-group-addon"><i className="fa fa-braille"></i></span>
                                <input type="text" id="gender3" name="gender3" placeholder="Gender" className="form-control"/>
                            </div>
                        </div>
                        <div className="form-group">
                            <div className="input-group">
                                <span className="input-group-addon"><i className="fa fa-map-marker"></i></span>
                                <input type="text" id="Location3" name="location3" placeholder="Location" className="from-control"/>

                            </div>
                        </div>
                    </form>
                    </ModalBody>
                    <ModalFooter>
                        <Button color="primary" onClick={this.toggleLarge}>Save</Button>{' '}
                        <Button color="secondary" onClick={this.toggleLarge}>Cancel</Button>
                    </ModalFooter>
                </Modal>
            </div>
        )
    }
}

export default Tractorowners;
