import React, { Component } from 'react';
import { Link, Switch, Route, Redirect } from 'react-router-dom'
import Header from '../../components/Header/';
import Sidebar from '../../components/Sidebar/';
import Breadcrumb from '../../components/Breadcrumb/';
import Aside from '../../components/Aside/';
import Footer from '../../components/Footer/';


import Dashboard from '../../views/Dashboard/'
import Request from '../../views/Request/'
import Farmers from '../../views/Farmers/'
import FBOs from '../../views/FBOs/'
import Tractors from '../../views/Tractors/'
import Tractorowners from '../../views/Tractorowners/'
import Operators from '../../views/Operators/'
import Tansactions from '../../views/Transactions/'
import Maps from '../../views/Maps/'



class Full extends Component {
  render() {
    return (
      <div className="app">
        <Header />
        <div className="app-body">
          <Sidebar {...this.props}/>
          <main className="main">
            <Breadcrumb />
            <div className="container-fluid">
              <Switch>
                <Route path="/dashboard" name="Dashboard" component={Dashboard}/>
                <Route path="/request" name="Request" component={Request}/>
                <Route path="/farmers" name="Farmers" component={Farmers}/>
                <Route path="/fbos" name="FBOs" component={FBOs}/>
                <Route path="/tractors" name="Tractors" component={Tractors}/>
                <Route path="/tractorowners" name="Tractorowners" component={Tractorowners}/>
                <Route path="/operators" name="Operators" component={Operators}/>
                <Route path="/transactions" name="Transactions" component={Tansactions}/>
                <Route path="/maps" name="Maps" component={Maps}/>
                <Redirect from="/" to="/dashboard"/>
              </Switch>
            </div>
          </main>
          <Aside />
        </div>
        <Footer />
      </div>
    );
  }
}

export default Full;
