import React, { Component } from 'react';
import { NavLink } from 'react-router-dom'

class Sidebar extends Component {

  handleClick(e) {
    e.preventDefault();
    e.target.parentElement.classList.toggle('open');
  }

  activeRoute(routeName) {
    return this.props.location.pathname.indexOf(routeName) > -1 ? 'nav-item nav-dropdown open' : 'nav-item nav-dropdown';
  }

  // secondLevelActive(routeName) {
  //   return this.props.location.pathname.indexOf(routeName) > -1 ? "nav nav-second-level collapse in" : "nav nav-second-level collapse";
  // }

  render() {
    return (
      <div className="sidebar">
        <nav className="sidebar-nav">
          <ul className="nav">
            <li className="nav-item">
              <NavLink to={'/dashboard'} className="nav-link" activeClassName="active"><i className="fa fa-gears"></i> Dashboard </NavLink>
            </li>
            <li className="nav-item">
              <NavLink to={'/request'} className="nav-link" activeClassName="active"><i className="icon-magnet"></i>Request </NavLink>
            </li>
            <li className="nav-item">
              <NavLink to={'/farmers'} className="nav-link" activeClassName="active"><i className="icon-user"></i>Farmers </NavLink>
            </li>
            <li className="nav-item">
              <NavLink to={'/fbos'} className="nav-link" activeClassName="active"><i className="icon-people "></i>FBOs</NavLink>
            </li>
            <li className="nav-item">
              <NavLink to={'/tractors'} className="nav-link" activeClassName="active"><i className="fa fa-truck"></i>Tractors </NavLink>
            </li>
            <li className="nav-item">
              <NavLink to={'/operators'} className="nav-link" activeClassName="active"><i className="fa fa-truck"></i>Tractor Operators </NavLink>
            </li>
            <li className="nav-item">
              <NavLink to={'/tractorowners'} className="nav-link" activeClassName="active"><i className="fa fa-user-secret "></i>Tractor Owners </NavLink>
            </li>
            <li className="nav-item">
              <NavLink to={'/transactions'} className="nav-link" activeClassName="active"><i className="fa fa-money"></i>Transactions </NavLink>
            </li>
            <li className="nav-item">
              <NavLink to={'/maps'} className="nav-link" activeClassName="active"><i className="fa fa-map-marker"></i>Map </NavLink>
            </li>

          </ul>
        </nav>
      </div>
    )
  }
}

export default Sidebar;
