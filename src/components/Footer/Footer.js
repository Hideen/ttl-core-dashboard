import React, { Component } from 'react';

class Footer extends Component {
  render() {
    return (
      <footer className="app-footer">
        <a href="http://hideensolutions.io">Hideen Solution</a> &copy; 2017 creativeLabs.
        <span className="float-right">Powered by <a href="http://hideensolutions.io">HS</a></span>
      </footer>
    )
  }
}

export default Footer;
